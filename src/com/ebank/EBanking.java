package com.ebank;

import java.util.Scanner;

import com.ebank.admin.AdminOperations;
import com.ebank.user.UserOperations;

/**
 * This class is the starting point of Application
 * 
 * @author Team-E
 * 
 */
public class EBanking {

	/**
	 * main method is used to call startApp method to print the home page menu
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// calling startApp method
		new EBanking().startApp();

	}// end of main


	/**
	 * From this method execution onwards you can see the Application working. this
	 * method asks the user for user login or administrator login and navigates
	 * respectively.
	 * 
	 * calling this method prints home page menu
	 * 
	 * @throws InvalidInputException
	 */
	public void startApp() {

		// initializing the scanner class
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

		// calling the printStartPage method to print admin and user options
		EBanking.printStartPage();

		boolean flag = false;
		try {
			String userInput = scanner.next();
			switch (userInput) {
			case "1": {
				flag = true;
				new AdminOperations().adminNavigator();
				break;
			}
			case "2": {
				flag = true;
				
				new UserOperations().userLogin();
				break;
			}
			default:
				flag = false;
				System.out.println("You have Entered wrong choice.");
				break;
			}
		} catch (Exception e) {
			System.out.println("Invalid command.");
		}

		if (flag == false) {
			this.startApp();
		}

	}// end of startApp
	/**
	 * This method is responsible for printing the content of starting page For user
	 * understanding.
	 */
	public static void printStartPage() {
		System.out.println("1) Admin");
		System.out.println("2) AccountUser");

	}// end of printStartPage

}
