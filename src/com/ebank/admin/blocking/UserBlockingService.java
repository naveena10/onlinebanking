package com.ebank.admin.blocking;

import java.util.Map;
import java.util.Scanner;

public class UserBlockingService {

	public static final String BLOCK_LIST = "UserBlockList.txt";

	/**
	 * This method adds a user to the list
	 * @param accNum
	 */
	public void addUserToStatusList(String accNum) {
		UserBlockingRepository userBlock = new UserBlockingRepository();
		Map<String, Boolean> blockList = userBlock.userFileReader(BLOCK_LIST);
		blockList.put(accNum, false);
		userBlock.userFileWriter(blockList, BLOCK_LIST);
	} //End of addUser
	
	
	
	/**
	 * This method checks whether user blocked or not
	 * 
	 * @param accNum
	 * @return if user Blocked returns true else false
	 */
	public boolean isBlocked(String accNum) {
		UserBlockingRepository userBlock = new UserBlockingRepository();
		Map<String, Boolean> blockList = userBlock.userFileReader(BLOCK_LIST);
		boolean flag = false;
		try {
			flag = blockList.get(accNum);
		} catch (Exception e) {

		}
		return flag;
	} // End of isBlocked

	/**
	 * This method mark the user as blocked or un_blocked
	 * 
	 * @param accNum
	 */
	public void block_OR_Unblock_User(String accNum) {
		boolean flag = false;
		UserBlockingRepository userBlockRepoObj = new UserBlockingRepository();
		Map<String, Boolean> blockList = userBlockRepoObj.userFileReader(BLOCK_LIST);
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		while (!flag) {
			System.out.println("1)BLOCK_USER\n2)UN-BLOCK_USER\n3)DON'T CHANGE");
			switch (scanner.next()) {
			case "1":
				blockList.replace(accNum, true);
				System.out.println("successfully blocked..");
				flag = true;
				break;
			case "2":
				blockList.replace(accNum, false);
				System.out.println("successfully unBlocked..");
				flag = true;
				break;
			case "3":
				flag = true;
				return;
			default:
				System.out.println("you choose wrong..");
				flag = false;
				break;
			}
		} // End of while

		userBlockRepoObj.userFileWriter(blockList, BLOCK_LIST);
	} // End of blockUser

	/**
	 * This method displays all the users Status weather blocked or not
	 */
	public void displayUserStatus() {
		UserBlockingRepository userBlock = new UserBlockingRepository();
		Map<String, Boolean> blockList = userBlock.userFileReader(BLOCK_LIST);
		System.out.println("user status :");
		for (Map.Entry<String, Boolean> userEntry : blockList.entrySet()) {
			try {
				String status = userEntry.getValue() ? "BLOCKED" : "ACTIVE";
				System.out.println(userEntry.getKey() + " : " + status);
			} catch (Exception e) {

			}
		}
	} // End of displayUserStatus

}
