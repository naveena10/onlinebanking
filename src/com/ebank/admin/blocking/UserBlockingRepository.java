package com.ebank.admin.blocking;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class UserBlockingRepository {
	
	/**
	 * This method writes map object into the file
	 * @param blockList
	 * @param fileName
	 * @return
	 */
	public boolean userFileWriter(Map<String, Boolean> blockList, String fileName) {
		boolean flag = false;	
		try {
			FileOutputStream outStream = new FileOutputStream(new File(fileName));
			ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);
			objOutStream.writeObject(blockList);
			objOutStream.flush();
			objOutStream.close();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;

	}// end of userFileWriter
	
	/**
	 * This method reads specific file and returns map Object
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Boolean> userFileReader(String fileName) {
		Map<String, Boolean> userFile = null;
		try {
			FileInputStream inStream = new FileInputStream(fileName);
			ObjectInputStream objInStream = new ObjectInputStream(inStream);
			userFile = (Map<String, Boolean>) objInStream.readObject();
			objInStream.close();
			inStream.close();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return (userFile == null) ? new HashMap<>() : userFile;
	}// end of userFileReader

}
