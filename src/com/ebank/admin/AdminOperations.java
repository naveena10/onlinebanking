package com.ebank.admin;

import java.util.Map;
import java.util.Scanner;

import com.ebank.EBanking;
import com.ebank.admin.blocking.UserBlockingService;
import com.ebank.repo.AdminRepository;
import com.ebank.repo.UserRepository;
import com.ebank.user.User;
import com.ebank.user.UserOperations;
import com.ebank.user.usercomplaints.UserComplaintsService;

/**
 * This class Contains functionalities, which an Administrator can do.
 * 
 * @author Team-E
 *
 */
@SuppressWarnings("resource")
public class AdminOperations {
	/**
	 * This method is used to Register an administrator with validations.
	 */
	public void adminRegistration() {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		// creating the reference object for admin class
		Admin adminObj = new Admin();
		// creating reference object for Validations class
		Validations validate = new Validations();
		boolean flag = false;
		try {
			while (!flag) {
				System.out.println("Enter name");
				adminObj.setAdminName(sc.next());
				flag = validate.nameValidator(adminObj.getAdminName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter Email");
				adminObj.setAdminMailId(sc.next());
				flag = validate.emailValidator(adminObj.getAdminMailId());
			}
			flag = false;
			while (!flag) {
				System.out.println("Set your AdminId");
				adminObj.setAdminId(sc.next());
				if(new AdminRepository().isAdmin(adminObj.getAdminId())) {
					System.out.println("Admin id already exist.Please enter a new Admin Id");
					flag=false;
				} else {
				flag = validate.userNameValidator(adminObj.getAdminId());
				}
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter Phone Number");
				adminObj.setAdminPhNum(sc.nextLong());
				flag = validate.phoneNumValidator(Long.toString(adminObj.getAdminPhNum()));
			}
			flag = false;
			while (!flag) {
				System.out.println("Set Password");
				adminObj.setPassword(sc.next());
				flag = validate.adminPasswordValidate(adminObj.getPassword());
			}

			new AdminRepository().addNewAdmin(adminObj.getAdminId(), adminObj);

		} catch (Exception e) {
			System.out.println("Invalid data");
		}

	} // end of adminRegistration

	/**
	 * This method is used to update a particular field for admin
	 * 
	 * @param admin
	 * @return
	 */
	public Admin updateAdminNavigator(Admin admin) {
		// initializing the scanner class

		Scanner sc = new Scanner(System.in);
		try {
			boolean flag = true;
			while (flag) {
				this.displayPageOfAdmin(admin);
				String input = sc.next();
				switch (input) {
				case "1":
					System.out.println("enter name");
					admin.setAdminName(sc.next());
					break;
				case "2":
					System.out.println("enter phone Number");
					admin.setAdminPhNum(sc.nextLong());
					break;
				case "3":
					System.out.println("enter mailId");
					admin.setAdminMailId(sc.next());
					break;

				case "4":
					System.out.println("enter password");
					admin.setPassword(sc.next());
					break;
				case "5":
					flag = false;
					break;
				default:
					System.out.println("you choose invalid option");
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return admin;
	} // end of updateAdminNavigator

	/**
	 * This method is used for updating the admin details by call update admin
	 * navigator method
	 */
	public void updateAdminDetails(String adminId) {
		// creating the reference object for adminRepository class
		AdminRepository adminRepo = new AdminRepository();

		try {
			Admin admin = null;
			Map<String, Admin> adminMap = adminRepo.adminFileReader();
			boolean flag = false;
			while (!flag) {
				admin = adminMap.get(adminId);

				flag = true;
				if (admin == null) {
					flag = false;
					System.out.println("AdminId not exist.");
				}
			}

			Admin newAdmin = this.updateAdminNavigator(admin);
			adminRepo.updateAdminDetails(newAdmin.getAdminId(), newAdmin);
		} catch (Exception e) {
			e.printStackTrace();
		}

	} // end of updateAdminDetails

	/**
	 * This method is used to print content of update page of Admin
	 * 
	 * @param admin
	 */
	public void displayPageOfAdmin(Admin admin) {
		System.out.println("To update the details please choose respective Option");
		System.out.println("1)AdminName : " + admin.getAdminName());
		System.out.println("2)Admin phone number : " + admin.getAdminPhNum());
		System.out.println("3)Admin mailId : " + admin.getAdminMailId());
		System.out.println("4)Password : " + admin.getPassword());
		System.out.println("5)Update");
	} // end of displayPageOfAdmin

	/**
	 * This method asks to the admin for id and password. It checks whether
	 * credentials and navigates respectively
	 */
	public void adminLogin() {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		// creating the reference object for admin class
		Admin admin = new Admin();
		AdminRepository adminRepo = new AdminRepository();
		Map<String, Admin> adminMap = adminRepo.adminFileReader();
		try {
			System.out.println("enter AdminId :");
			admin.setAdminId(sc.next());
			boolean flag = new AdminRepository().isAdmin(admin.getAdminId());
			if (flag) {
				System.out.println("enter Admin password :");
				admin.setPassword(sc.next());
				flag = adminMap.get(admin.getAdminId()).getPassword().equals(admin.getPassword()) ? true : false;
				if (flag) {

					printAfterLoginPage(admin.getAdminId());
				} else {
					int cnt = 0;
					int flag1 = 0;
					// forgotPassword(admin.getAdminId());
					while (cnt < 3) {
						System.out.println("Password is incorrect, Try again");
						String password = sc.next();
						if (adminMap.get(admin.getAdminId()).getPassword().equals(password)) {

							System.out.println("Login Successful....");
							printAfterLoginPage(admin.getAdminId());
							flag1++;

							break;
						}
						cnt++;
					}
					if (flag1 == 0) {
						System.out.println("Forgot Password?want to reset?Y/N");
						String c = sc.next();
						if (c.equals("Y")) {
							String psw = AdminRepository.Random(admin.getAdminId());
							System.out.println("Your password is" + psw);
							adminLogin();

						}
					}
				}
			} else {
				System.out.println("you entered invalid credentials.");
				adminLogin();
			}

		} catch (Exception e) {

		}
	}

	// end of adminLogin

	/**
	 * With this method admin can add user by using user registration method
	 */
	public void addUser() {
		// creating the reference object for user-operations class
		UserOperations user_operations = new UserOperations();
		user_operations.userRegistration();

	}// end of addUser

	/**
	 * This method is responsible for admin logout and navigates to EBanking
	 * homePage.
	 */
	public void adminLogout() {
		System.out.println("you are successfully Loggedout.");
		new EBanking().startApp();

	}// end of adminLogout

	/**
	 * This method is an utility method used to print content of Admin home page.
	 */
	public void printAdminHomePage() {

		System.out.println("1)Register ");
		System.out.println("2)Login ");

	}// end of printAdminOperations

	/**
	 * This method is used to navigation purpose based on admin input commands
	 */
	public void adminNavigator() {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		// calling method to print the home page
		printAdminHomePage();
		boolean flag = false;
		try {
			String input = sc.next();
			switch (input) {
			case "1":
				flag = true;
				adminRegistration();
				break;
			case "2":
				flag = true;
				adminLogin();
				break;
			default:
				flag = false;
				System.out.println("You have Entered wrong choice.");
				break;
			}

		} catch (Exception e) {

			System.out.println("Invalid Input...");
		}

		if (flag == false)
			this.adminNavigator();
	} // end of adminNavigator

	/**
	 * This method is an utility method prints content after Admin Login.
	 */
	public void printAfterLoginPage(String adminId) {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		boolean flag = true;
		while (flag) {
			System.out.println("1)Add new User");
			System.out.println("2)Update Admin Details");
			System.out.println("3)Display User Details");
			System.out.println("4)Display user Complaints");
			System.out.println("5)Block or Unblock the user");
			System.out.println("6)Display blocked list");
			System.out.println("7)admin Logout");

			String choice = sc.next();
			
				switch (choice) {
				case "1":
					addUser();
					break;
				case "2":
					updateAdminDetails(adminId);
					break;
				case "3":
					this.displayUserDetails();
					break;
				case "4":
					new UserComplaintsService().displayComplaints();
					break;
				case "5":
					String accnum;
					System.out.println("enter account number to block or unblock :");
					accnum=sc.next();
					boolean userflag=new UserRepository().isUser(accnum);
					if (userflag) {
						new  UserBlockingService().block_OR_Unblock_User(accnum);
					} else {
						System.out.println("given account number doesn't exist");
					}
					break;
				case "6":
					new  UserBlockingService().displayUserStatus();
					break;
				case "7":
					adminLogout();
					break;

				default:
					System.out.println("Please enter a valid choice");
				
			}
		}
	} // end of printAfterLoginPage

	/**
	 * This method is used to display user Details
	 */
	public void displayUserDetails() {
		Map<String, User> userFile = new UserRepository().userFileReader();
		for(Map.Entry<String, User> userEntry : userFile.entrySet()) {
			User userObj = userEntry.getValue();
			System.out.println("customer Account Number : "+userObj.getAccountNumber());
			System.out.println("customer Name : "+userObj.getName());
			System.out.println("gender: "+userObj.getGender());
			System.out.println("password: "+userObj.getPassword());
			System.out.println("Balance: "+userObj.getBalance());
			System.out.println("Date of Birth: "+userObj.getDateOfBirth());
			System.out.println("customer EmailId: "+userObj.getUserMailId());
			System.out.println("customer phone Number: "+userObj.getUserPhNum());
			System.out.println("===============================");
		}
	}
}
