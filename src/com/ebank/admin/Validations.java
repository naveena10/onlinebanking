package com.ebank.admin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class contains methods which are used to validate Admin details
 * 
 * @author Team-E
 *
 */
public class Validations {

	private static final String EMAIL_PATTERN = "^[a-zA-Z0-9]+@gmail.com";
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})";
	private static final String ADMIN_USERNAME = "[a-zA-Z0-9]{6,10}";
	private static final String NAME_PATTERN = "^[a-zA-Z]+";
	private static final String PHONE_NUMBER_PATTERN = "^[789][0-9]{9}";
	private static final String Account_Number_PATTERN = "^[0-9]{6,8}";

	/**
	 * This Method is used to validate the email
	 * 
	 * @return true if its valid or else false
	 * @param userEmail
	 */
	public boolean emailValidator(String email) {
		Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matchuserEmailPattern = emailPattern.matcher(email);
		return matchuserEmailPattern.matches();
	} // end of emailValidator

	/**
	 * This Method is used to validate the account number
	 * 
	 * @return true if its valid or else false
	 * @param userAccountNumber
	 */
	public boolean userAccountNumberValidator(String accountNumber) {
		Pattern accountNumberPattern = Pattern.compile(Account_Number_PATTERN);
		Matcher matchuserAccountNumberPattern = accountNumberPattern.matcher(accountNumber);
		return matchuserAccountNumberPattern.matches();
	} // end of accountNumberValidator

	/**
	 * This Method is used to validate the username
	 * 
	 * @return true if its valid or else false
	 * @param userName
	 */
	public boolean userNameValidator(String userName) {
		Pattern userNamePattern = Pattern.compile(ADMIN_USERNAME);
		Matcher matchUserName = userNamePattern.matcher(userName);
		return matchUserName.matches();
	} // end of userNameValidator

	/**
	 * This Method is used to validate the password of admin
	 * 
	 * @param adminPassword
	 * @return true if its valid or else false
	 */
	public boolean adminPasswordValidate(String adminPassword) {
		Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matchPassword = passwordPattern.matcher(adminPassword);
		if (matchPassword.matches()) {
			return true;
		}
		return false;
	}

	/**
	 * This Method is used to validate the password of user
	 * 
	 * @param userPassword
	 * @return true if its valid or else false
	 */
	public boolean userPasswordValidate(String userPassword) {
		Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matchPassword = passwordPattern.matcher(userPassword);
		if (matchPassword.matches()) {
			return true;
		}
		return false;
	}

	/**
	 * This method is used to validate name
	 * 
	 * @param name
	 * @return true if it is valid or else false
	 */
	public boolean nameValidator(String name) {
		Pattern namePattern = Pattern.compile(NAME_PATTERN);
		Matcher matchName = namePattern.matcher(name);
		return matchName.matches();
	} // end of nameValidator

	/**
	 * This method is used to validate phone number
	 * 
	 * @param phNum
	 * @return true if it is valid or else false
	 */
	public boolean phoneNumValidator(String phNum) {
		Pattern phNumPattern = Pattern.compile(PHONE_NUMBER_PATTERN);
		Matcher matchphNum = phNumPattern.matcher(phNum);
		return matchphNum.matches();
	} // end of phoneNumValidator

}
