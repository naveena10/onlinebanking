package com.ebank.admin;

import java.io.Serializable;

/**
 * @author Team-E This class is an Utility class representing Admin objects And
 *         implements Serializable interface
 *
 */
public class Admin implements Serializable {

	private static final long serialVersionUID = 1L;
	private String adminId;
	private String password;
	private String adminName;
	private long adminPhNum;
	private String adminMailId;

	public String getAdminMailId() {
		return adminMailId;
	}

	public void setAdminMailId(String adminMailId) {
		this.adminMailId = adminMailId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public long getAdminPhNum() {
		return adminPhNum;
	}

	public void setAdminPhNum(long adminPhNum) {
		this.adminPhNum = adminPhNum;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", password=" + password + ", adminName=" + adminName + ", adminPhNum="
				+ adminPhNum + ", adminMailId=" + adminMailId + "]";
	}

}
