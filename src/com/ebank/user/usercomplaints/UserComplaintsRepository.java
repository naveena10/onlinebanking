package com.ebank.user.usercomplaints;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class deals with file Storage regarding userComplaints
 * @author Team-E
 *
 */
public class UserComplaintsRepository {
	
	/**
	 * This method used to Store userComplaints in a file
	 * @param userMap
	 * @return
	 */
	public boolean userComplaintsFileWriter(Map<String, ArrayList<String>> userMap) {
		boolean flag = false;
		try {
			//open the file
			FileOutputStream outStream = new FileOutputStream(new File("UserComplaints.txt"));
			ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);
			objOutStream.writeObject(userMap);
			objOutStream.flush();
			//close the file
			objOutStream.close();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;

	}// end of userComplaintsFileWriter
	 
	/**
	 * This method is used to read user complaints file 
	 * @return Map<String, ArrayList<String>> type Object
	 */
	@SuppressWarnings("unchecked")
	public Map<String, ArrayList<String>> userComplaintsFileReader() {
		Map<String, ArrayList<String>> userFile = null;
		try {
			//open the file
			FileInputStream inStream = new FileInputStream("UserComplaints.txt");
			ObjectInputStream objInStream = new ObjectInputStream(inStream);
			userFile = (Map<String, ArrayList<String>>) objInStream.readObject();
			objInStream.close();
			//close the file
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (userFile == null) ? new HashMap<>() : userFile;
	}// end of userCompalintsFileReader
	
}
