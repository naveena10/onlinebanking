package com.ebank.user.usercomplaints;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

/**
 * This class have functionalities like ariseComplaint and display complaints
 * etc.
 * 
 * @author Team-E
 *
 */
public class UserComplaintsService {

	/**
	 * This method asks user to write complaint and calls repository method
	 * 
	 * @param accNum
	 */
	public void ariseComplaint(String accNum) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("please, write your complaint :");
		String complaint = sc.nextLine();
		UserComplaintsRepository complaintRepo = new UserComplaintsRepository();
		Map<String, ArrayList<String>> complaintMap = complaintRepo.userComplaintsFileReader();
		ArrayList<String> complaintList = complaintMap.get(accNum);
		if(complaintList == null) {
			complaintList = new ArrayList<>();
		}
		complaintList.add(complaint);
		complaintMap.put(accNum, complaintList);
		boolean flag = new UserComplaintsRepository().userComplaintsFileWriter(complaintMap);
		if(flag)
		{
			System.out.println("Complaint is successfully sent");
		}
		else
		{
			System.out.println("Something went wrong");
		}
	} // End of ariseComplaint

	/**
	 * This method used to display Complaints of a user
	 * 
	 * @param accNum
	 */
	public void displayComplaints() {
		UserComplaintsRepository complaintRepo = new UserComplaintsRepository();
		Map<String, ArrayList<String>> userComplaints = complaintRepo.userComplaintsFileReader();
		for (Map.Entry<String, ArrayList<String>> entry : userComplaints.entrySet()) {
			System.out.println(entry.getKey() +" "+entry.getValue());
		}
		//System.out.println(userComplaints);
	} // End of displayComplaints

}
