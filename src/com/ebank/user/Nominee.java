package com.ebank.user;

import java.io.Serializable;

/**
 * 
 * @author Batch-E This class is an Utility class representing Nominee objects
 *         And implements Serializable interface
 */
public class Nominee implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nomineeName;
	private String nomineeAccountNum;

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeAccountNumber() {
		return nomineeAccountNum;
	}

	public void setNomineeAccountNumber(String nomineeAccountNum) {
		this.nomineeAccountNum = nomineeAccountNum;
	}

	@Override
	public String toString() {
		return ("\nNomineeName=" + nomineeName + "\nNomineeAccountNum=" + nomineeAccountNum);
	}
}
