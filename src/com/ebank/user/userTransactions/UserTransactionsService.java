package com.ebank.user.userTransactions;

import java.util.Date;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;

/**
 * This class have a functionalities of a user transactions like funds transfer, withdraw, deposit etc..
 * @author TEAM-E
 *
 */
public class UserTransactionsService {
	
	/* Note : All these functionalities we can perform during user Login session only
	 *       so that we have to carry user details along the session.
	 */
	
	public void FundsTransfer(Scanner sc ) {
		
	}
	
	public void isValidAccount() {
		
	}
	
	public void balanceEnquiry() {
		
	}
	
	public void miniStatement(String accNum) {
		NavigableMap<Date, UserTransactionsPojo> userTxList = 
				new UserTransactionRepository().getTransactionsFile(accNum);
		if(userTxList.size()>0) {
			for(Map.Entry<Date,UserTransactionsPojo> entry : userTxList.entrySet()) {
				  UserTransactionsPojo pojo = entry.getValue();
				  pojo.setDateOfTransaction(entry.getKey());
				}
		}
		//userTxList
	}
	
	public void displayTransactions(UserTransactionsPojo pojo) {
		System.out.println();
	}
	
	/*
	 * whenever we perform deposit or withdraw we should record transaction details in file
	 * 
	 */
	
	public void depositeAmount() {
		
		//Ask the user that how much he wants to deposit
		//modify the balance of user
		//and store it on map and file again
		
	} //End of depositeAmount
	
	public void withdrawAmount() {
		
		//Ask the user that how much he wants to withdraw
		//check balance whether he had enough funds or not
		//if No : give message about his balance and ask to re-enter
		//if yes : deduct amount from his balance
		
	}
	
}
