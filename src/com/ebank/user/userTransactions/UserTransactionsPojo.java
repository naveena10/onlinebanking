package com.ebank.user.userTransactions;

import java.io.Serializable;
import java.util.Date;

/**
 * This is a POJO class representing object containing user transaction details.
 * 
 * @author TEAM-E
 *
 */
public class UserTransactionsPojo implements Serializable {

	private static final long serialVersionUID = 1L;
	private int accountNum;
	private Date dateOfTransaction;
	private String narration;
	private double debitAmount;
	private double creditAmount;

	// setters and getters
	public int getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(int accountNum) {
		this.accountNum = accountNum;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	@Override
	public String toString() {
		return "UserTransactionsPojo [accountNum=" + accountNum + "\n dateOfTransaction=" + dateOfTransaction
				+ "\n narration=" + narration + "\n debitAmount=" + debitAmount + "\n creditAmount=" + creditAmount + "]";
	}

}
