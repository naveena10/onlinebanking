package com.ebank.user.userTransactions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * This class have functionalities which deals with repositories of user
 * transactions
 * 
 * @author Team-E
 *
 */
public class UserTransactionRepository {

	/**
	 * This method writes a transactions to the user transactions file
	 * 
	 * @param txPojo
	 */
	public void addTransactionToFile(UserTransactionsPojo txPojo) {
		UserTransactionRepository userTxRepo = new UserTransactionRepository();
		NavigableMap<Date, UserTransactionsPojo> userTxObj = userTxRepo
				.getTransactionsFile(Integer.toString(txPojo.getAccountNum()));
		userTxObj.put(txPojo.getDateOfTransaction(), txPojo);
		Map<String, NavigableMap<Date, UserTransactionsPojo>> userFile = userTxRepo.userFileReader();
		userFile.put(Integer.toString(txPojo.getAccountNum()), userTxObj);
		userTxRepo.userFileWriter(userFile);
	} // End of addTransactionToFile

	/**
	 * This method returns specific user transaction list based on accNum
	 * 
	 * @param accNum
	 * @return map object containing Calendar and UserTransactions object
	 */
	public TreeMap<Date, UserTransactionsPojo> getTransactionsFile(String accNum) {
		Map<String, NavigableMap<Date, UserTransactionsPojo>> txList = this.userFileReader();
		TreeMap<Date, UserTransactionsPojo> userTx = (TreeMap<Date, UserTransactionsPojo>) txList.get(accNum);

		return (userTx == null) ? new TreeMap<Date, UserTransactionsPojo>() : userTx;
	} // End of getTransactionsFile

	/**
	 * This method reads the file and returns Map object containing Account number
	 * as key and User object as values
	 */
	@SuppressWarnings("unchecked")
	public Map<String, NavigableMap<Date, UserTransactionsPojo>> userFileReader() {
		Map<String, NavigableMap<Date, UserTransactionsPojo>> userFile = null;
		try {
			FileInputStream inStream = new FileInputStream("UserTxDetails.txt");
			ObjectInputStream objInStream = new ObjectInputStream(inStream);
			userFile = (Map<String, NavigableMap<Date, UserTransactionsPojo>>) objInStream.readObject();
			objInStream.close();
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (userFile == null) ? new HashMap<String, NavigableMap<Date, UserTransactionsPojo>>() : userFile;
	}// end of userFileReader

	/**
	 * This method stores the map object-userMap in file and returns boolean value
	 * 
	 * @param userMap
	 * @return
	 */
	public boolean userFileWriter(Map<String, NavigableMap<Date, UserTransactionsPojo>> userTxMap) {
		boolean flag = false;
		try {
			FileOutputStream outStream = new FileOutputStream(new File("UserTxDetails.txt"));
			ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);
			objOutStream.writeObject(userTxMap);
			objOutStream.flush();
			objOutStream.close();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;

	}// end of userFileWriter

}
