package com.ebank.user.userTransactions;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.TreeMap;

import com.ebank.repo.UserRepository;
import com.ebank.user.User;

/**
 * @author Team-E This class contains functionalities, which admin can do
 *
 */
public class BankOperations {

	/**
	 * This method is used to select the admin operations
	 * 
	 * @param admin
	 */
	static int rateOfInterestPercentage = 4;
	public void operations(String accnum) {
		System.out.println("1)deposit\n2)withdraw\n3)transfer\n4)Rate Of Intrest\n5)Transaction History\n6)Mini Statement\n7)exit");
		System.out.println("Enter your choice");
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		String choice = sc.next();
		switch (choice) {
		case "1":
			deposit(accnum);
			break;
		case "2":
			withdraw( accnum);
			break;
		case "3":
			transferMoney(accnum);
			break;
		case "4":
			rateOfInterest(accnum);
			break;
		case "5":
			transactionHistory(accnum);
			break;
		case "6":
			miniStatement(accnum);
			break;
		case "7":
			System.out.println("You have successfully logged out");
			break;
		default:
			System.out.println("Enter a valid choice");

		}// end of switch
	}// end of operations method

	/**
	 * This method is used to deposit and display the balance
	 * 
	 * @param admin
	 */
	public void deposit( String accnum) {
		// creating transaction object and assigning account number
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		UserTransactionsPojo txPojo = new UserTransactionsPojo();
		txPojo.setAccountNum(Integer.parseInt(accnum));
		UserRepository rep = new UserRepository();
		Map<String, User> userFile = rep.userFileReader();
		User userObj = userFile.get(accnum);
		System.out.println("Enter the money to deposit");
		double amount = (int) sc.nextDouble();
		if (amount > 0) {
			System.out.println(userObj.getBalance());
			// setting amount to Transaction Object
			txPojo.setCreditAmount(amount);
			// asking the user to write some narration
			System.out.println("please, enter the reason of transaction");
			txPojo.setNarration(sc.next());
			// setting date to txPojo Object
			Calendar date = Calendar.getInstance();
			txPojo.setDateOfTransaction(date.getTime());
			// calling method addTransactionToFile of UserTransactionRepository class
			new UserTransactionRepository().addTransactionToFile(txPojo);
			amount = userObj.getBalance() + amount;
			userObj.setBalance(amount);
			userFile.replace(accnum, userObj);
			boolean flag = rep.userFileWriter(userFile);
			;
			if (flag) {
				System.out.println("Balance is:" + userObj.getBalance());
			}
		} else {
			System.out.println("Money cannot be deposited");
		}
	}// end of deposit

	/**
	 * withdraw method for user
	 * 
	 * @param sc
	 * @param accnum
	 */
	public void withdraw(String accnum) {
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		// creating transaction object and assigning account number
		UserTransactionsPojo txPojo = new UserTransactionsPojo();
		txPojo.setAccountNum(Integer.parseInt(accnum));
		UserRepository rep = new UserRepository();
		Map<String, User> userFile = rep.userFileReader();
		User userObj = userFile.get(accnum);
		System.out.println("Enter the money:");
		double amount = (int)sc.nextDouble();
		if (amount > 0 && amount < userObj.getBalance()) {

			// setting amount to Transaction Object
			txPojo.setDebitAmount(amount);
			// asking the user to write some narration
			System.out.println("please, enter the reason of transaction");
			String narration = sc.next();
			txPojo.setNarration(narration);
			// setting date to txPojo Object
			Calendar date = Calendar.getInstance();
			txPojo.setDateOfTransaction(date.getTime());
			// calling method addTransactionToFile of UserTransactionRepository class
			new UserTransactionRepository().addTransactionToFile(txPojo);

			amount = userObj.getBalance() - amount;
			userObj.setBalance(amount);
			userFile.replace(accnum, userObj);
			boolean flag = rep.userFileWriter(userFile);
			if (flag) {
				System.out.println("remaining Balance is:" + userObj.getBalance());
			}
		} // end of if condition
		else {
			System.out.println("Insufficient Balance.Money cannot be withdrawn");
		}
	}// end of withdraw method

	public void BalanceEnquiry(String accnum) {
		
		UserRepository rep = new UserRepository();
		Map<String, User> userFile = rep.userFileReader();
		User userObj = userFile.get(accnum);
		System.out.println("Balance is:" + userObj.getBalance());
	}
	
	/**
	 * This method reads the transactions list from file and calls a method to print transaction details of user
	 * @param accNum
	 */
	public void transactionHistory(String accnum) {
		NavigableMap<Date, UserTransactionsPojo> userTxList = 
				new UserTransactionRepository().getTransactionsFile(accnum);
		if(userTxList.size()>0) {
			for(Map.Entry<Date,UserTransactionsPojo> entry : userTxList.entrySet()) {
				UserTransactionsPojo pojo = entry.getValue();
				//pojo.setDateOfTransaction(entry.getKey());
				this.displayTransactions(pojo);
			}
		}
		//userTxList
	} //End of transactionHistory
	/**
	 * This method is used to print mini statement.This method prints last five transactions of user
	 * @param accnum
	 */
	public void miniStatement(String accnum) {
		
	NavigableMap<Date, UserTransactionsPojo> userTxList = new TreeMap<Date, UserTransactionsPojo>(Collections.reverseOrder()); 
	userTxList.putAll(new UserTransactionRepository().getTransactionsFile(accnum));
	
	if(userTxList.size()>0) {
			int count = 0;
			for(Map.Entry<Date,UserTransactionsPojo> entry : userTxList.entrySet()) {
				UserTransactionsPojo pojo = entry.getValue();
				//pojo.setDateOfTransaction(entry.getKey());
				this.displayTransactions(pojo);
				count++;
				if(count==5)
					break;
			}
		}
		//userTxList
	} //End of transactionHistory
	
	
	/**
	 * This method is used to display Transaction details of user
	 * @param pojo
	 */
	public void displayTransactions(UserTransactionsPojo pojo) {
		System.out.println(pojo.getDateOfTransaction()
				+" reason: "+pojo.getNarration()+" credit: "+pojo.getCreditAmount()+" debit: "+pojo.getDebitAmount());
	} //End of displayTransactions
	 public void billPayment(String accnum)
	 {
		 @SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		 System.out.println("1)Current Bill\n2)Phone bill\n3)Water Bill\n4)Back");
		System.out.println("Enter your choice");
		String choice=sc.next();
		switch(choice)
		{
		case "1":
			withdraw(accnum);
			break;
		case "2" :
			withdraw(accnum);
			break;
		case "3":
			withdraw(accnum);
			break;
		case "4":
			break;
		default:
			System.out.println("Invalid choice");
			
		}
			
	 }
	 /**
	  * this method calculate the rate of interest for saving accounts
	  * @param accnum
	  */
		public static void rateOfInterest(String accnum)
		{
		
			UserRepository rep = new UserRepository();
			Map<String, User> userFile = rep.userFileReader();
			User userObj = userFile.get(accnum);
			double balance = userObj.getBalance();
			double rateofint=(rateOfInterestPercentage/(100.0));
			double	balance1 = (balance*rateofint)/365;
			balance = balance+balance1;
			System.out.println("rate of interest:"+ balance);
			userObj.setBalance(balance);
			userFile.replace(accnum, userObj);
			
			boolean flag=rep.userFileWriter(userFile);;
			if(flag)
			{
				System.out.println("Balance is:"+userObj.getBalance());
			}
		}//end of rateOfIntrest method
			
			
	/**
	 * 	This method is used to transfer money from one account to another account
	 * @param fromaccnum
	 */
	 public void transferMoney(String fromaccnum) {
			UserRepository userRepo = new UserRepository();
			Map<String, User> userMap = userRepo.userFileReader();
			@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
			boolean flag = false;
			UserTransactionsPojo txPojo1 = new UserTransactionsPojo();
			UserTransactionsPojo txPojo2 = new UserTransactionsPojo();
			while (!flag) {
				System.out.println("Enter account number to be transfered");
				String toAccNum = sc.next();
				flag = userMap.containsKey(toAccNum);
				txPojo2.setAccountNum(Integer.parseInt(toAccNum));
			}

			User userObj1 = userMap.get(fromaccnum);
			User userObj2 = userMap.get(Integer.toString(txPojo2.getAccountNum()));
			txPojo1.setAccountNum(Integer.parseInt(userObj1.getAccountNumber()));
			double amount = 0.0d;
			flag = false;
			while (!flag) {
				System.out.println("Enter the money to transfer");
				amount = (int)sc.nextDouble();
				flag = (amount > 0 && userObj1.getBalance() >= amount) ? true : false;
			}

			// setting amount to Transaction Object
			txPojo1.setDebitAmount(amount);
			txPojo2.setCreditAmount(amount);
			// setting date to txPojo Object
			Calendar date = Calendar.getInstance();
			txPojo1.setDateOfTransaction(date.getTime());
			txPojo2.setDateOfTransaction(date.getTime());
			// calling method addTransactionToFile of UserTransactionRepository class
			UserTransactionRepository txRepo = new UserTransactionRepository();
			txRepo.addTransactionToFile(txPojo1);
			txRepo.addTransactionToFile(txPojo2);
			
			userObj1.setBalance(userObj1.getBalance() - amount);
			
			userObj2.setBalance(userObj2.getBalance() + amount);

			userMap.replace(userObj1.getAccountNumber(), userObj1);
			userMap.replace(userObj2.getAccountNumber(), userObj2);

			flag = userRepo.userFileWriter(userMap);

			if (flag) {
				System.out.println("Remaining balance is:" + userObj1.getBalance());
			} else {
				System.out.println("Money cannot be transferred");
			}

		}//end of transferMoney method
}//end of class

