package com.ebank.user;

import java.util.Calendar;
import java.util.Map;
import java.util.Scanner;

import com.ebank.EBanking;
import com.ebank.admin.Validations;
import com.ebank.admin.blocking.UserBlockingService;
import com.ebank.repo.AdminRepository;
import com.ebank.repo.UserRepository;
import com.ebank.user.userTransactions.BankOperations;
import com.ebank.user.usercomplaints.UserComplaintsService;

/**
 * @author Team-E This class contains functionalities, which a user can do
 *
 */
public class UserOperations {
	/**
	 * This method is used to login the user
	 *
	 */

	public void userLogin() {
		// initializing the scanner class
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		// creating the reference object for admin class
		User user = new User();
		UserRepository userRepo = new UserRepository();
		Map<String, User> userMap = userRepo.userFileReader();
		try {
			boolean flag = true;
			System.out.println("enter accountNumber :");
			user.setAccountNumber(sc.next());
			if (new UserBlockingService().isBlocked(user.getAccountNumber())) {
				System.out.println("This Account Number is blocked");
				flag = false;
			} else {
				flag = new UserRepository().isUser(user.getAccountNumber());
				if (flag) {
					System.out.println("enter User password :");
					user.setPassword(sc.next());
					flag = userMap.get(user.getAccountNumber()).getPassword().equals(user.getPassword()) ? true : false;
					if (flag) {
						printAfterLoginPage(user.getAccountNumber());
					} else {
						int cnt = 0;
						int flag1 = 0;
						// while loop is executed for 3 times if password is incorrect
						while (cnt < 3) {
							System.out.println("Password is incorrect, Try again");
							String password = sc.next();
							if (userMap.get(user.getAccountNumber()).getPassword().equals(password)) {
								System.out.println("Login Successful....");
								printAfterLoginPage(user.getAccountNumber());
								flag1++;
								break;
							}
							cnt++;
						}
						if (flag1 == 0) {
							System.out.println("Forgot Password?want to reset?Y/N");
							String c = sc.next();
							if (c.equals("Y")) {
								String psw = AdminRepository.Random(user.getAccountNumber());
								System.out.println("Your password is" + psw);
								userLogin();

							}
						}
					}
				} else {
					System.out.println("you entered invalid credentials.");
					userLogin();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// end of userLogin

	/**
	 * This method is used to update a particular field in user details
	 * 
	 * @param user
	 * @return
	 */
	public User updateUserNavigator(User user) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		try {
			boolean flag = true;
			while (flag) {
				this.displayPageOfUser(user);
				String input = sc.next();
				switch (input) {
				case "1":
					System.out.println("enter name");
					user.setName(sc.next());
					break;
				case "2":
					System.out.println("enter phone Number");
					user.setUserPhNum(sc.nextLong());
					break;
				case "3":
					System.out.println("enter mailId");
					user.setUserMailId(sc.next());
					break;

				case "4":
					System.out.println("enter password");
					user.setPassword(sc.next());
					break;
				case "5":
					String accnum = user.getNominee().getNomineeAccountNumber();
					System.out.println("enter Nominee name");
					Nominee nominee = new Nominee();
					nominee.setNomineeName(sc.next());
					nominee.setNomineeAccountNumber(accnum);
					user.setNominee(nominee);
					break;
				case "6":
					String name = user.getNominee().getNomineeName();
					System.out.println("enter Nominee account number");
					Nominee nominee1 = new Nominee();
					nominee1.setNomineeAccountNumber(sc.next());
					nominee1.setNomineeName(name);
					user.setNominee(nominee1);
					break;
				case "7":
					flag = false;
					break;
				default:
					System.out.println("you choose invalid option");
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	} // end of updateUserNavigator

	/**
	 * this method is used to update user details
	 * 
	 * @param accnum
	 */
	public void updateUserDetails(String accnum) {
		UserRepository userRepo = new UserRepository();

		try {
			User user = null;
			Map<String, User> userMap = userRepo.userFileReader();
			boolean flag = false;
			while (!flag) {
				user = userMap.get(accnum);
				flag = true;
				if (user == null) {
					flag = false;
					System.out.println("Account number does not exist.");
				}
			}
			User newUser = this.updateUserNavigator(user);
			userRepo.updateUserDetails(newUser.getAccountNumber(), newUser);

		} catch (Exception e) {
			e.printStackTrace();
		}
	} // end of updateAdminDetails

	/**
	 * This method is used to print content of update page of Admin
	 * 
	 * @param admin
	 */
	public void displayPageOfUser(User user) {
		System.out.println("To update the details please choose respective Option");
		System.out.println("Account number : " + user.getAccountNumber());
		System.out.println("1)User Name : " + user.getName());
		System.out.println("2)user phone number : " + user.getUserPhNum());
		System.out.println("3)user mailId : " + user.getUserMailId());
		System.out.println("4)Password : " + user.getPassword());
		System.out.println("5)Nominee name : " + user.getNominee().getNomineeName());
		System.out.println("6)Nominee account number : " + user.getNominee().getNomineeAccountNumber());
		System.out.println("7)Update");
	} // end of displayPageOfAdmin

	public void userLogout() {
		System.out.println("you are successfully Loggedout.");
	}// end of userLogout

	public void userRegistration() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		User userObj = new User();
		Validations validate = new Validations();
		Nominee nomineeObj = new Nominee();
		boolean flag = false;
		try {
			while (!flag) {
				System.out.println("Enter name of the user");
				userObj.setName(sc.next());
				flag = validate.nameValidator(userObj.getName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Set your Account number");
				userObj.setAccountNumber(sc.next());
				if (new UserRepository().isUser(userObj.getAccountNumber())) {
					System.out.println("Account Number already exist.Please enter a new Account Number");
					flag = false;
				} else {
					flag = validate.userAccountNumberValidator(userObj.getAccountNumber());
				}
			}
			
			//setting date of Birth
			userObj.setDateOfBirth(new DateOfBirth().askUserDOB());
			//setting date of account opening
			userObj.setDateOfAccountOpening(Calendar.getInstance().getTime());
			
			flag = false;
			while(!flag) {
				System.out.println("choose gender 1)male\n2)female");
				switch(sc.next()) {
				case "1":
					userObj.setGender('m');
					flag = true;
					break;
				case "2":
					userObj.setGender('f');
					flag = true;
					break;
				default:
					System.out.println("you choose wrong");
					break;
				}
			}
			
			flag = false;
			while (!flag) {
				System.out.println("Enter Email");
				userObj.setUserMailId(sc.next());
				flag = validate.emailValidator(userObj.getUserMailId());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter the name of nominee");
				nomineeObj.setNomineeName(sc.next());
				userObj.setNominee(nomineeObj);
				flag = validate.nameValidator(userObj.getNominee().getNomineeName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter the account number of nominee");
				nomineeObj.setNomineeAccountNumber(sc.next());
				userObj.setNominee(nomineeObj);
				flag = validate.nameValidator(userObj.getNominee().getNomineeName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter Phone Number");
				userObj.setUserPhNum(sc.nextLong());
				flag = validate.phoneNumValidator(Long.toString(userObj.getUserPhNum()));
			}
			flag = false;
			while (!flag) {
				System.out.println("Set Password");
				userObj.setPassword(sc.next());
				flag = validate.userPasswordValidate(userObj.getPassword());
			}
			System.out.println("Set balance");
			userObj.setBalance(sc.nextDouble());
			new UserRepository().addNewUser(userObj.getAccountNumber(), userObj);
		} catch (Exception e) {
			System.out.println("Invalid data");
		}
	}
	// end of userRegistration

	/**
	 * This method is an utility method used to print content of User home page.
	 */
	public void printUserHomePage() {

		System.out.println("Login ");

	}// end of printUserOperations

	/**
	 * This method is an utility method prints content after User Login.
	 */
	public void printAfterLoginPage(String accnum) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = true;
		while (flag) {
			System.out.println(
					"1)Balance Enquiry\n2)Update Details\n3)Account Information\n4)Bill Payment\n5)Bank Operations\n6)Help\n7)Logout");
			System.out.println("Enter your choice");
			String choice = sc.next();
			switch (choice) {
			case "1":
				new BankOperations().BalanceEnquiry(accnum);
				break;
			case "2":
				updateUserDetails(accnum);
				break;
			case "3":
				new UserRepository().displayUserDetails(accnum);
				break;
			case "4":
				new BankOperations().billPayment(accnum);
				break;
			case "5":
				new BankOperations().operations(accnum);
				break;
			case "6":
				new UserComplaintsService().ariseComplaint(accnum);
				break;
			case "7":
				userLogout();
				new EBanking().startApp();
				flag = false;
				break;

			default:
				System.out.println("please enter a valid option");
			}
		}
	} // end of printAfterLoginPage
}
