package com.ebank.user;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is an utility class representing User Objects which implements
 * Serialazation
 * 
 * @author Team-E
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private char gender;
	private String accountNumber;
	private String password;
	private Date dateOfBirth;
	private Date dateOfAccountOpening;
	private long userPhNum;
	private String userMailId;
	private Nominee nominee;
	private double balance;
	private double rateOfInterest;

	//setters and getters
	
	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public Date getDateOfAccountOpening() {
		return dateOfAccountOpening;
	}

	public void setDateOfAccountOpening(Date dateOfAccountOpening) {
		this.dateOfAccountOpening = dateOfAccountOpening;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public long getUserPhNum() {
		return userPhNum;
	}

	public void setUserPhNum(long userPhNum) {
		this.userPhNum = userPhNum;
	}

	public String getUserMailId() {
		return userMailId;
	}

	public void setUserMailId(String userMailId) {
		this.userMailId = userMailId;
	}

	public Nominee getNominee() {
		return nominee;
	}

	public void setNominee(Nominee nominee) {
		this.nominee = nominee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", gender=" + gender + ", password=" + password + ", accountNumber="
				+ accountNumber + ", dateOfBirth=" + dateOfBirth + ", dateOfAccountOpening=" + dateOfAccountOpening
				+ ", userPhNum=" + userPhNum + ", userMailId=" + userMailId + ", nominee=" + nominee + ", balance="
				+ balance + "]";
	}

}
