package com.ebank.repo;

//importing the predefined packages
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

//importing the user defined packages
import com.ebank.admin.Admin;
import com.ebank.admin.AdminOperations;

/**
 * This class contains methods which deals with Administrator related data
 * manipulation.
 * 
 * @author Team-E
 *
 */
public class AdminRepository {
	/**
	 * This method checks weather credentials are matched or not.
	 * 
	 * @param adminId
	 * @return flag
	 */
	public boolean isAdmin(String adminId) {
		// creating the object for adminRepository class
		AdminRepository adminRepo = new AdminRepository();
		// storing the file data into map
		Map<String, Admin> adminMap = adminRepo.adminFileReader();
		boolean flag = false;
		if (adminMap.containsKey(adminId)) {
			flag = true;
		}

		return flag;
	}// end of isAdmin

	/**
	 * This method is used to add new Admin to the file
	 * 
	 * @param adminId
	 * @param newAdmin
	 */
	public void addNewAdmin(String adminId, Admin newAdmin) {
		// storing the file data into map
		Map<String, Admin> adminFile = this.adminFileReader();
		boolean flag = false;
		if (adminFile != null) {
			adminFile.put(adminId, newAdmin);
			flag = this.adminFileWriter(adminFile);
		} else {
			System.out.println("registering is unsuccessful..");
		}

		if (flag) {
			System.out.println("Registration is sucessful..");
			new AdminOperations().adminNavigator();
		} else {

			System.out.println("updation is unsucessful");
		}

	} // end of addNewAdmin

	/**
	 * This method replaces the adminObject that maps to the respective adminId with
	 * updated admin Object
	 * 
	 * @param adminId
	 * @param adminObj
	 */
	public void updateAdminDetails(String adminId, Admin adminObj) {
		AdminRepository adminRepo = new AdminRepository();
		Map<String, Admin> adminFile = adminRepo.adminFileReader();
		boolean flag = false;
		if (adminFile != null) {
			adminFile.replace(adminId, adminObj);
			flag = adminRepo.adminFileWriter(adminFile);
		} else {
			System.out.println("AdminId does not exist");
		}
		if (flag) {
			System.out.println("updation is sucess..");
			new AdminOperations().printAfterLoginPage(adminId);
		} else {
			System.out.println("updation is unsucessful");
		}

	}// end of updateAdminDetails

	/**
	 * This method used to store the map object containing Admin objects in file And
	 * returns boolean value
	 * 
	 * @param userMap
	 * @return flag
	 */
	public boolean adminFileWriter(Map<String, Admin> adminMap) {
		boolean flag = false;
		try {
			FileOutputStream outStream = new FileOutputStream(new File("AdminDetails.txt"));
			ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);
			objOutStream.writeObject(adminMap);
			objOutStream.flush();
			objOutStream.close();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;

	}// end of adminFileWriter

	/**
	 * This method reads the file and returns Map object containing AdminId as key
	 * And Admin objects as values
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Admin> adminFileReader() {
		Map<String, Admin> adminFile = null;
		try {
			FileInputStream inStream = new FileInputStream("AdminDetails.txt");
			ObjectInputStream objInStream = new ObjectInputStream(inStream);
			adminFile = (Map<String, Admin>) objInStream.readObject();
			objInStream.close();
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (adminFile == null) ? new HashMap<>() : adminFile;
	} // end of adminFile

	/**
	 * This method retrieves data form file based on adminId
	 * 
	 * @param adminId
	 */
	public void displayAdminDetails(String adminId) {
		Map<String, Admin> adminFile = new AdminRepository().adminFileReader();
		if (adminFile != null) {
			Admin adminObj = adminFile.get(adminId);
			System.out.println(adminObj);
		} else {
			System.out.println("Admin Id does not exist");
		}
	}// end of displayAdminDetails

	@SuppressWarnings("resource")
	public static String Random(String adminId) {
		// creating the object for login class
		// Login login = new Login();
		// creating the reference variable for random method
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		int otp = rd.nextInt(1000000);
		System.out.println(otp);
		if (otp < 0) {
			otp = otp * (-1);
		}
		System.out.println("enter the otp");
		int enterOtp = sc.nextInt();
		if (enterOtp == otp) {
			AdminRepository adminRepo = new AdminRepository();
			Map<String, Admin> adminMap = adminRepo.adminFileReader();
			return adminMap.get(adminId).getPassword();
		} else {
			System.out.println("wrong otp!!!");
		}

		return "";
	}
}
