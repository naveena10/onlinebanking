package com.ebank.repo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import com.ebank.admin.AdminOperations;
import com.ebank.admin.blocking.UserBlockingService;
import com.ebank.user.User;

/**
 * This class deals with repositories belong to user, like user details, user
 * transaction details etc.
 * 
 * @author Team-E
 *
 */
public class UserRepository {

	/**
	 * This method checks weather credentials are matched or not.
	 * 
	 * @param account number
	 * @return flag
	 */
	public boolean isUser(String accountNumber) {
		UserRepository userRepo = new UserRepository();
		Map<String, User> userMap = userRepo.userFileReader();
		boolean flag = false;
		if (userMap.containsKey(accountNumber)) {

			flag = true;
		}

		return flag;
	}// end of isUser

	/**
	 * This method is used to add a new user to the file
	 * 
	 * @param accountNum
	 * @param newUser
	 */
	public void addNewUser(String accountNum, User newUser) {
		Map<String, User> userFile = this.userFileReader();
		boolean flag = false;
		if (userFile != null) {
			userFile.put(accountNum, newUser);
			flag = this.userFileWriter(userFile);
			new UserBlockingService().addUserToStatusList(accountNum);
		} else {

			System.out.println("registering is unsuccessful..");
		}
		if (flag) {
			System.out.println("Registration is success..");
			new AdminOperations().printAfterLoginPage(accountNum);
		} else {
			System.out.println("Registration is unsuccessful");
		}
	}// end of addNewUser

	/**
	 * This method replaces the userObject that maps to the respective account
	 * number with updated user Object
	 * 
	 * @param accNum
	 * @param userObj
	 */
	public void updateUserDetails(String accNum, User userObj) {
		Map<String, User> userFile = this.userFileReader();
		boolean flag = false;
		if (userFile != null) {
			userFile.replace(accNum, userObj);
			flag = this.userFileWriter(userFile);
		} else {
			System.out.println("account number does not exist");
		}
		if (flag) {
			System.out.println("updation is success..");
		} else {
			System.out.println("updation is unsuccessful");
		}

	}// end of updateUserDetails

	/**
	 * This method retrieves data form file based on user Account Number
	 * 
	 * @param accNum
	 */
	public void displayUserDetails(String accNum) {
		Map<String, User> userFile = this.userFileReader();

		if (userFile != null) {
			User userObj = userFile.get(accNum);
			System.out.println("customer Account Number : "+userObj.getAccountNumber());
			System.out.println("customer Name : "+userObj.getName());
			System.out.println("gender: "+userObj.getGender());
			System.out.println("password: "+userObj.getPassword());
			System.out.println("Balance: "+userObj.getBalance());
			System.out.println("Date of Birth: "+userObj.getDateOfBirth());
			System.out.println("customer EmailId: "+userObj.getUserMailId());
			System.out.println("customer phone Number: "+userObj.getUserPhNum());
			System.out.println("=====================================");
		} else {
			System.out.println("account number does not exist");
		}
	}// end of displayUserDetails

	/**
	 * This method reads the file and returns Map object containing Account number
	 * as key and User object as values
	 */
	@SuppressWarnings("unchecked")
	public Map<String, User> userFileReader() {
		Map<String, User> userFile = null;
		try {
			FileInputStream inStream = new FileInputStream("UserDetails.txt");
			ObjectInputStream objInStream = new ObjectInputStream(inStream);
			userFile = (Map<String, User>) objInStream.readObject();
			objInStream.close();
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (userFile == null) ? new HashMap<>() : userFile;
	}// end of userFileReader

	/**
	 * This method stores the map object-userMap in file and returns boolean value
	 * 
	 * @param userMap
	 * @return flag
	 */
	public boolean userFileWriter(Map<String, User> userMap) {
		boolean flag = false;
		try {
			FileOutputStream outStream = new FileOutputStream(new File("UserDetails.txt"));
			ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);
			objOutStream.writeObject(userMap);
			objOutStream.flush();
			objOutStream.close();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;

	}// end of userFileWriter

	@SuppressWarnings("resource")
	public static String Random(String accountNumber) {
		// creating the object for login class
		// Login login = new Login();
		// creating the reference variable for random method
		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		int otp = rd.nextInt(1000000);
		System.out.println(otp);
		if (otp < 0) {
			otp = otp * (-1);
		}
		System.out.println("enter the otp");
		int enterOtp = sc.nextInt();
		if (enterOtp == otp) {
			UserRepository userRepo = new UserRepository();
			Map<String, User> userMap = userRepo.userFileReader();
			return userMap.get(accountNumber).getPassword();

		} else {
			System.out.println("wrong otp!!!");

		}
		return "";

	}
}
